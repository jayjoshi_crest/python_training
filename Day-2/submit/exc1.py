import re
import sys
def getFreq(line,ch):

	#
	#	function to count given character in string
	#
	
	return line.count(str(ch))
	

def validate(line,choice):
	
	#
	#	function to validate the input
	#	choice is the input to select which validation should take place in input
	#	choice ->
	#			1. String
	#			2. Integer
	#			3. Char


	if choice == 1:
		return line
	elif choice == 2:
		val = -1
		try:
			val = int(line)
		except ValueError:
			print('please enter an integer')
			sys.exit(1)
		return val
	elif choice == 3:
		if(len(line) != 1):
			print('enter a single char only')
			sys.exit(1)

		return line




if __name__ == '__main__':
	line = validate(raw_input("enter a string : "),1)
	ch = validate(raw_input("enter a character : "),3)
	print getFreq(line,ch)
	
