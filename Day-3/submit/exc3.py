from exc1 import getlist
import sys
def check(list1):
	
	list2 = []
	try:
		for each in list1:
			list2.append(int(each))
	except ValueError:
		print("enter only arithmetic values")
		sys.exit(1)
		
	return list2

def av(list1):

	list1 = check(list1)

	sum = 0

	for each in list1:
		sum += each

	avg = sum/len(list1)

	return avg

if __name__ == '__main__':
	list1 = getlist()
	print "average : "+str(av(list1))