import sys

def getkey(index,choice):
	return index[choice]


N=0
try:
	N = int(raw_input("enter number of students : "))
except ValueError:
	print("enter integer only ")
	sys.exit(1)

students = []
for i in range(N):
	stud = []
	stud.append(raw_input("enter first name : "))
	stud.append(raw_input("enter last name  : "))
	stud.append(raw_input("enter roll no    : "))

	students.append(stud)

while(True):
	try:
		par = int(raw_input("select parameter : \n1.first name\n2.second name\n3.roll no\n4.exit\n==>"))
	except ValueError:
		print("enter integer only")
		sys.exit(1)

	if(par<=0 or par >4):
		break

	students=sorted(students,key=lambda x:x[par-1])

	print students