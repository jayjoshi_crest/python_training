import re
def word_count():
    phrase = raw_input("enter a string : ")
    dictn  = {}
    phrase = phrase.replace('\t',' ').replace(' \'',' ').replace('\' ',' ').lower()

    for line in phrase.split('\n'):
	    for each in re.sub('[^a-zA-Z \'1-9]',' ',line).split(' '):
	    	if each == '' or each == ' ':
	    		continue
	    	if each in dictn:
	   			dictn[each]+=1;
	    	else:
	    		dictn[each] = 1;

    print dictn

if __name__=='__main__':
	word_count()