import re,sys
from iptools.ipv6 import validate_ip
file = open("newfile.txt")
wholefile = file.read()
matchObj = re.match('(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$',wholefile)

if matchObj:
    try:
        for each in matchObj.groups():
            if(int(each) > 255):
                print('invalid')
                sys.exit(1)
    except:
        print('invalid')
        sys.exit(1)
    print wholefile
    
else:
    
    matchObj = re.match('([0-9A-F]{0,4}:){7}[0-9A-F]{0,4}',wholefile)
    if matchObj:
        print matchObj.group()

    else:
        
        if validate_ip(wholefile):
            print wholefile + " -- by iptools"
        else:
            print "invalid"